------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

minetest.register_privilege("maidroid", { S("Can") ..  " " .. S("administer any maidroid"),
	give_to_singleplayer = false,
	on_grant = function(name, granter_name)
		minetest.chat_send_player(name, S("You gained privilege: ") .. S("administer any maidroid"))
	end,
	on_revoke = function(name, granter_name)
		minetest.chat_send_player(name, S("You lost privilege: ") .. S("administer any maidroid"))
	end,
})

minetest.register_craft{
	output = "maidroid:maidroid_egg",
	recipe = {
		{"default:coalblock", "default:mese"          , "default:coalblock"},
		{""                 , "maidroid_tool:nametag" , ""},
		{""                 , "default:bronzeblock"   , ""},
	},
}

minetest.register_craft{
	output = "maidroid_tool:nametag",
	recipe = {
		{""                , "farming:cotton", ""},
		{"default:paper"   , "default:paper" , "default:paper"},
		{"default:tin_lump", "dye:black"     , "default:copper_ingot"},
	},
}

if minetest.settings:get_bool("maidroid_enable_capture_rod", true) then
	minetest.register_craft{
		output = "maidroid_tool:capture_rod",
		recipe = {
			{"wool:blue"          , "dye:red"            , "default:mese_crystal"},
			{""                   , "default:steel_ingot", "dye:red"},
			{"default:steel_ingot", ""                   , "wool:violet"},
		},
	}
end
-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
