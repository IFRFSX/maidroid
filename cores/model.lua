------------------------------------------------------------
-- Copyleft (Я) 2021 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

-- Core interface functions
local on_start, on_pause, on_resume, on_stop, on_step, is_tool

local wander =  maidroid.cores.wander

on_start = function(self)
	wander.on_start(self)
end

on_resume = function(self)
	wander.on_resume(self)
end

on_stop = function(self)
	wander.on_stop(self)
end

on_pause = function(self)
	wander.on_pause(self)
end

is_tool = function(stack, stacks)
	if stack:get_name() == "model:tool" then
		return true
	end
	return false
end

local function on_step(self, dtime, moveresult)
	wander.on_step(self, dtime, moveresult)
end

maidroid.register_core("model", {
	description	= _S("model"),
	on_start	= on_start,
	on_stop		= on_stop,
	on_resume	= on_resume,
	on_pause	= on_pause,
	on_step		= on_step,
	is_tool		= is_tool,
})

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
