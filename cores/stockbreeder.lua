------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

local job_pause = tonumber(minetest.settings:get("maidroid_job_pause_time")) or 4

-- Core interface functions
local on_start, on_pause, on_resume, on_stop, on_step, is_tool

local wander =  maidroid.cores.wander
local known_shearable = {}
local known_milkable = {}
local known_killable = {}

if maidroid.mods.petz then
	table.insert(known_milkable, "petz:calf")
	table.insert(known_milkable, "petz:camel")
	table.insert(known_milkable, "petz:goat")
	table.insert(known_shearable, "petz:lamb")
	table.insert(known_killable, "petz:ducky")
	table.insert(known_killable, "petz:hen")
	table.insert(known_killable, "petz:chicken")
	-- do not instert rooster as they will fight to death
end

if maidroid.mods.animalia then
	table.insert(known_milkable, "animalia:cow")
	table.insert(known_shearable, "animalia:sheep")
	table.insert(known_killable, "animalia:chicken")
	table.insert(known_killable, "animalia:turkey")
end

if maidroid.mods.animal then
	table.insert(known_milkable, "mobs_animal:cow")
	for _, color in ipairs(dye.dyes) do
		table.insert(known_shearable, "mobs_animal:sheep_"..color[1])
	end
	table.insert(known_killable, "mobs_animal:chicken")
end

on_start = function(self)
	self.encountered_milkables = self.encountered_milkables or {}
	wander.on_start(self)
	self.timers.skip = 0
	self.job_pause = 0
end

on_resume = function(self)
	self:set_tool(self.selected_tool)
	self.timers.skip = 0
	self.job_pause = 0
	wander.on_resume(self)
end

on_stop = function(self)
	wander.on_stop(self)
	self.job_pause = nil
end

on_pause = function(self)
	wander.on_pause(self)
	self.job_pause = nil
end

is_tool = function(stack, stacks)
	local name = stack:get_name()
	if name == "petz:shears" or name == "bucket:bucket_empty" then
		return true
	end
	return false
end

local function set_animation_for_job(self, itemname, animal)
	self:set_animation(maidroid.animation_frames.MINE)
	self.object:set_velocity{x=0, y=0, z=0}
	self.job_pause = job_pause
	self:set_tool(itemname)
	if animal then
		local dir = vector.direction(self:get_pos(),
			animal.object:get_pos())
		self:set_yaw_by_direction(dir)
		self.target_obj = animal.object
	end
end

local function get_nearest_entity(self, names, radius, count)
	local pos = self:get_pos()
	local objects = minetest.get_objects_inside_radius(pos, radius)
	local wield_entity = self.wield_item:get_luaentity()
	local min_dist, dist = radius + 1, 0
	local ret

	for _, obj in pairs(objects) do
		local luaentity = obj:get_luaentity()
		if not obj:is_player() and luaentity and luaentity ~= self
			and luaentity ~= wield_entity then
			for _,name in ipairs(names) do
				if luaentity.name == name then
					dist = vector.distance(pos, obj:get_pos())

					if dist < min_dist then
						min_dist = dist
						ret = luaentity
					end
				end
			end
		end
	end
	return ret, objects
end

function count_entities(self, list, name)
	local count = 0
	local wield_entity = self.wield_item:get_luaentity()
	for _, obj in pairs(list) do
		local luaentity = obj:get_luaentity()
		if not obj:is_player() and luaentity and luaentity ~= self
			and luaentity ~= wield_entity then
			if luaentity.name == name then
				count = count + 1
			end
		end
	end
	return count
end

local function select_follow_item(inv, follow, modname)
	local items = follow
	if modname == "petz" then
		items = follow:gsub(" ", ""):split(",")
	end
	if not items then
		return
	end
	for _,item in ipairs(items) do
		if inv:contains_item("main", item) then
			return item
		end
	end
end

local function milk_cows(self)
	local inv = self:get_inventory()
	local animal = get_nearest_entity(self, known_milkable, 2.5)

	if not animal then
		return
	end

	local gametime = minetest.get_gametime()
	local idx
	for i, table in ipairs(self.encountered_milkables) do
		if table[1] == animal then
			idx = i
			break
		end
	end
	if not idx then
		table.insert(self.encountered_milkables, { animal, animal.name:split(":")[1], gametime - 10, 0})
		idx = #self.encountered_milkables
	end
	local encountered_milkable = self.encountered_milkables[idx]
	-- Milk the animal
	local criterion
	if encountered_milkable[2] == "petz" then
		criterion = not animal.is_male and not animal.is_baby
			and not animal.milked and animal.owner == self.owner_name
	elseif encountered_milkable[2] == "animalia" then
		criterion = not animal.child and not animal.gotten
	elseif  encountered_milkable[2] == "mobs_animal" then
		criterion = not animal.child and not animal.gotten and animal.owner == self.owner_name
	end
	if criterion and inv:contains_item("main", "bucket:bucket_empty") then
		inv:remove_item("main", ItemStack("bucket:bucket_empty"))
		if not inv:contains_item("main", "bucket:bucket_empty") then
			self.need_core_selection = true
		end
		local stack
		if encountered_milkable[2] == "mobs_animal" then
			animal.gotten = true
			stack = ItemStack("mobs:bucket_milk")
		elseif encountered_milkable[2] == "petz" then
			minetest.sound_play("petz_"..animal.type.."_moaning", {
				max_hear_distance = petz.settings.max_hear_distance,
				pos = animal.object:get_pos(),
				object = animal.object,
			})
			animal.food_count = mobkit.remember(animal, "food_count", 0)
			animal.milked = mobkit.remember(animal, "milked", true)
			stack = ItemStack(encountered_milkable[2]..":bucket_milk")
		elseif encountered_milkable[2] == "animalia" then
			encountered_milkable[4] = 0
			animal.gotten = mobkit.remember(animal, "gotten", true)
			stack = ItemStack(encountered_milkable[2]..":bucket_milk")
		end
		minetest.add_item(self:get_pos(), stack)

		set_animation_for_job(self, "bucket:bucket_empty", animal)
		return true
	end
	-- Feed the animal if time elapsed since last feed is over threshold
	local feed_item
	if animal.follow and gametime - encountered_milkable[3] > 3 then
		feed_item = select_follow_item(inv, animal.follow, encountered_milkable[2])
	end
	if feed_item then
		if encountered_milkable[2] == "petz" and animal.milked then -- Just heal do not touch food counters
			animal.food_count = mobkit.remember(animal, "food_count", animal.food_count + 1)
			mobkit.heal(animal, animal.max_hp * petz.settings.tamagochi_feed_hunger_rate)
			petz.refill(animal)
		elseif encountered_milkable[2] == "animalia" and encountered_milkable[4] < 5 then
			encountered_milkable[4] = encountered_milkable[4] + 1
			mobkit.heal(animal, animal.max_hp)
			if not animal.breeding and animal.breeding_cooldown <= 0 then
				animal.breeding = true
			end
		elseif encountered_milkable[2] == "mobs_animal" and animal.gotten then
			animal.health = math.min(animal.hp_max, animal.health + 4)
			animal.food = (animal.food or 0) + 1
			if animal.food >= 8 then
				animal.food = 0
				animal.gotten = false
			end
		else
			return
		end
		encountered_milkable[3] = gametime
		inv:remove_item("main", feed_item)
		set_animation_for_job(self, feed_item, animal)
		return true
	end
end

local function shear_sheeps(self)
	local inv = self:get_inventory()
	local animal = get_nearest_entity(self, known_shearable, 2.5)

	if not animal or inv:contains_item("main", "petz:shears")
		or not ( inv:contains_item("main", "mobs:shears")
		or inv:contains_item("main", "animalia:shears") ) then
		return
	end

	local mod = animal.name:split(":")[1]
	if mod == "petz" and not animal.shaved and animal.owner == self.owner_name then
		petz.lamb_wool_shave(animal, self)
	elseif mod == "animalia" and not animal.child and not animal.gotten then
		minetest.add_item( self.object:get_pos(), ItemStack( "wool:white " .. math.random(1, 3)))
		animal.gotten = mobkit.remember(animal, "gotten", true)
		animal.dye_color = mobkit.remember(animal, "dye_color", "white")
		animal.dye_hex = mobkit.remember(animal, "dye_hex",  "#abababc000")
		animal.object:set_properties({ textures = {"animalia_sheep.png"}})
	elseif mod == "mobs_animal" and not animal.child and not animal.gotten and animal.owner == self.owner_name then
		minetest.add_item( self.object:get_pos(), ItemStack( "wool:" .. animal.name:split(":")[2]:split("_")[2] .. " " .. math.random(1, 3)))
		animal.object:set_properties({ textures = {"mobs_sheep_shaved.png"}, mesh = "mobs_sheep_shaved.b3d"})
		animal.gotten = true
	else
		return
	end

	if mod == "mobs_animal" then
		set_animation_for_job(self, "mobs:shears", animal)
	else
		set_animation_for_job(self, mod .. ":shears", animal)
	end
end

local function has_group_item(self, group)
	local stacks = self:get_inventory():get_list("main")

	for index, stack in ipairs(stacks) do
		if minetest.get_item_group(stack:get_name(), group) > 0 then
			return stack:get_name()
		end
	end
end

local random_drop = function(pos, name, chance, amount)
	if not chance or chance == 1 or math.random(chance) == 1 then
		local item_stack = ItemStack(name)
		if amount then
			item_stack:set_count(amount)
		end
		minetest.add_item(pos, item_stack)
	end
end

local kill_poultries = function(self, animal_object)
	local animal = animal_object:get_luaentity()
	if not animal then
		return
	end
	local mod = animal.name:split(":")[1]
	if mod == "petz" and
		( animal.owner == nil or animal.owner == ""
		or animal.owner == self.owner_name ) then
		if animal.type == "ducky" then
			random_drop(self:get_pos(), "petz:ducky_feather", 3)
			random_drop(self:get_pos(), "petz:raw_ducky", 3)
		else
			random_drop(self:get_pos(), "petz:raw_chicken", 3)
		end
		random_drop(self:get_pos(), "petz:bones", 6)
		mobkit.clear_queue_high(animal)
		minetest.sound_play("petz_default_punch",
			{object = animal.object, gain = 0.5,
			max_hear_distance = petz.settings.max_hear_distance })
	elseif mod == "animalia" then
		mob_core.make_sound(self, "death")
		mobkit.clear_queue_low(animal)
		mob_core.item_drop(animal)
	elseif mod == "mobs_animal" then
		minetest.add_item(animal_object:get_pos(), ItemStack("mobs:chicken_feather " .. math.random(0,2)))
		minetest.add_item(animal_object:get_pos(), ItemStack("mobs:chicken_raw"))
	end
	animal_object:remove()
end

local can_kill_poultries = function(self)
	local sword = has_group_item(self, "sword") -- Can we kill poultries with sword
	if not sword then
		return
	end
	local animal, list = get_nearest_entity(self, known_killable, 15)
	if not animal or vector.distance(self:get_pos(), animal.object:get_pos()) > 2.5 then
		return -- There is no poultries close enough
	end
	local count = count_entities(self, list, animal.name)
	if animal.name == "petz:hen" then
		count = count + count_entities(self, list, "petz:chicken")
	elseif animal.name == "petz:chicken" then
		count = count + count_entities(self, list, "petz:hen")
	end
	if count < 8 then
		return -- Poultries population is too low here
	end
	local mod = animal.name:split(":")[1]
	if mod ~= "petz" and mod ~= "animalia" and mod ~= "mobs_animal" then
		return
	end
	self.action = kill_poultries
	set_animation_for_job(self, sword, animal)
	return true
end

local harvest_poop = function(self)
	if not maidroid.mods.petz then -- Do we have petz ?
		return
	end
	local shovel = has_group_item(self, "shovel") -- Do we have a shovel
	if not shovel then
		return
	end
	local poops = minetest.find_node_near(self.object:get_pos(), 3, "petz:poop", true)
	if not poops or minetest.is_protected(poops, self.owner_name) then
		return -- No diggable poop found
	end

	self:set_yaw_by_direction(vector.direction(self:get_pos(), poops))
	set_animation_for_job(self, shovel)
	self.job_pause = job_pause / 2
	-- Add to inventory and remove node
	self:add_items_to_main({ ItemStack("petz:poop") })
	minetest.remove_node(poops)
	return true
end

local function task(self, dtime, moveresult)
	if milk_cows(self) then
		return
	elseif shear_sheeps(self) then
		return
	elseif can_kill_poultries(self) then
		return
	else
		harvest_poop(self)
	end
end

local function on_step(self, dtime, moveresult)
	self:toggle_entity_jump(dtime, moveresult)

	if self.job_pause > 0 then -- A job currently done, stand and mine
		-- Check we have an existing target
		if self.target_obj and self.target_obj:get_luaentity() then
			self:set_yaw_by_direction(vector.direction(self:get_pos(), self.target_obj:get_pos()))
		end
		self.job_pause = self.job_pause - dtime
		if self.job_pause > 0 then
			return
		end

		-- We got an action to do now (only kill poultries for now)
		if self.target_obj and self.action then
			self.action(self, self.target_obj)
			self.action = nil
		end
		-- The rest takes end reset states and plan some wandering only cycles
		self:set_animation(maidroid.animation_frames.WALK)
		self:set_tool(self.selected_tool)
		self.timers.skip = job_pause
		self.target_obj = nil
	end

	self:pickup_item()

	if self.timers.skip > 0 then	-- A job has finished, rest a bit
		wander.on_step(self, dtime, moveresult,  nil, maidroid.criteria.is_fence, true)
		self.timers.skip = self.timers.skip - dtime
		if self.timers.skip < 0 then
			self.timers.walk = 0
		end
	else							-- Wander to look after cows
		wander.on_step(self, dtime, moveresult, task, maidroid.criteria.is_fence, true)
	end
end

maidroid.register_core("stockbreeder", {
	description	= S("a stockbreeder"),
	on_start	= on_start,
	on_stop		= on_stop,
	on_resume	= on_resume,
	on_pause	= on_pause,
	on_step		= on_step,
	is_tool		= is_tool,
	walk_max = 1.25 * maidroid.timers.walk_max,
})

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
