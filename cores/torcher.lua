------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyright (c) 2020 IFRFSX.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator
local on_start, on_pause, on_stop, on_step, is_tool

local timers = maidroid.timers
timers.place_torch_max = tonumber(minetest.settings:get("maidroid_torch_delay")) or 0.75
local follow = maidroid.cores.follow.on_step
local wander = maidroid.cores.wander.on_step

on_start = function(self)
	self.state = maidroid.states.FOLLOW
	self.timers.walk = 0
	self.timers.change_dir = 0
	self.timers.place_torch = 0
	self.is_placing = false
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.STAND)
end

on_stop = function(self)
	self.state = nil
	self.timers.place_torch = nil
	self.is_placing = nil
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.STAND)
end

on_pause = function(self)
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.SIT)
end

local function is_dark(pos)
	local light_level = minetest.get_node_light(pos)
	return light_level <= 5
end

on_step = function(self, dtime, moveresult)
	-- When owner offline the maidroid does nothing.
	local player = minetest.get_player_by_name(self.owner_name)

	self:pickup_item()

	-- When owner offline just wander
	if not player then
		wander(self, dtime, moveresult)
		return
	end

	-- Save state and try to follow
	local laststate = self.state
	follow(self, dtime, moveresult, player)

	-- Can't follow just wander
	if self.state == maidroid.states.IDLE and self.far_from_owner then
		if laststate ~= self.state then
			minetest.chat_send_player(self.owner_name, S("One torcher is too far away"))
		end
		wander(self, dtime, moveresult)
		return
	end

	if self.timers.place_torch >= timers.place_torch_max then
		if self.is_placing then
			if not self:get_inventory():contains_item("main", self.selected_tool) then
				self.need_core_selection = true
				return
			end
			if self.state == maidroid.states.IDLE then
				self:set_animation(maidroid.animation_frames.STAND)
			else
				self:set_animation(maidroid.animation_frames.WALK)
			end

			local pos = vector.round(self:get_pos())
			local stack = ItemStack(self.selected_tool)
			if not minetest.is_protected(pos, self.owner_name) then
				local r_stack, success = minetest.item_place_node(stack, player, {
					type = "node",
					under = vector.add(pos, vector.new(0,-1,0)),
					above = pos,
				})
				if success then
					self:get_inventory():remove_item("main", stack)
				end
			end
			self.is_placing = false
		else
			if is_dark(vector.round(self:get_pos())) then -- if it is dark, set torch
				self.is_placing = true
				if self.state == maidroid.states.IDLE then
					self:set_animation(maidroid.animation_frames.MINE)
				else
					self:set_animation(maidroid.animation_frames.WALK_MINE)
				end
			end
		end
		self.timers.place_torch = 0
	else
		self.timers.place_torch = self.timers.place_torch + dtime
	end
end

is_tool = function(stack, stacklist)
	if stack:get_name() == "default:torch" then
		return true
	end
	return false
end

-- register a definition of a new core.
maidroid.register_core("torcher", {
	description = S("a torcher"),
	on_start    = on_start,
	on_stop     = on_stop,
	on_resume   = on_start,
	on_pause    = on_pause,
	on_step     = on_step,
	is_tool     = is_tool,
})

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
