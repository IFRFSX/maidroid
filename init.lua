------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

maidroid = {}

maidroid.modname = minetest.get_current_modname()
maidroid.modpath = minetest.get_modpath(maidroid.modname)

if minetest.get_translator ~= nil then
	maidroid.translator = minetest.get_translator(maidroid.modname)
else
	maidroid.translator = function ( s ) return s end
end

if minetest.get_modpath("pipeworks") then
	maidroid.pipeworks = true
end

dofile(maidroid.modpath .. "/api.lua")
dofile(maidroid.modpath .. "/register.lua")
dofile(maidroid.modpath .. "/cores.lua")

dofile(maidroid.modpath .. "/tools/nametag.lua")
if minetest.settings:get_bool("maidroid_enable_capture_rod", true) then
	dofile(maidroid.modpath .. "/tools/capture_rod.lua")
end
-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
